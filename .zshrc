zstyle ':omz:update' mode disabled
export ZSH=/home/tbuchta/.oh-my-zsh

autoload -U colors && colors

ZSH_THEME="mortalscumbag"
ZSH_TMUX_UNICODE=true
ZSH_TMUX_AUTOSTART=false
ZSH_TMUX_AUTOQUIT=false
ZSH_TMUX_AUTOCONNECT=false
ZSH_DOTENV_PROMPT=false
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_no_bold[cyan]%}->"

plugins=(git vi-mode dotenv fzf fzf-tab)


LANG=en_US.utf8
LANGUAGE=en_US.utf8

source $ZSH/oh-my-zsh.sh

# Allow comments in commands
setopt interactivecomments

export EDITOR='nvim'
export ARCHFLAGS="-arch x86_64"
export GITHUB_USERNAME="tomasz-buchta"

# vi mode
bindkey -v
export KEYTIMEOUT=1

# use vim key bindings for tab-complete
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'

GPG_TTY=$(tty)
export GPG_TTY

. /opt/asdf-vm/asdf.sh

git_last_branches() {
  git reflog | awk '/checkout/ {print $NF}' | nl | awk 'NR==2 || !a[$2]++ {print $2}' | uniq
}
gch() {
  git checkout $(git_last_branches | fzf)
}
# Set JAVA_HOME
. ~/.asdf/plugins/java/set-java-home.zsh

export PATH="$PATH:/home/tbuchta/.cargo/bin:/home/tbuchta/scripts:/home/tbuchta/.local/bin"

alias tmux="env TERM=xterm-256color tmux"
alias config='/usr/bin/git --git-dir=/home/tbuchta/.cfg/ --work-tree=/home/tbuchta'
alias google-chrome="google-chrome-stable"
alias vimconfig="cd ~/.config/nvim/; vim ."
alias cfgap="config add -p"
alias cfgst="config status"
alias cfgc="config commit"
alias vim="nvim"
alias dc="docker-compose"
alias be="bundle exec"
alias to-nato="nato --convert"
alias weather="curl wttr.in"
alias cat="bat"

# Kubernetes
alias k=kubectl
complete -F __start_kubectl k

# Disable for now since it slows down startup time
# Minikube
# source $HOME/.shell-completions/minikube_completions.sh

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
[[ /usr/local/bin/kubectl ]] && source <(kubectl completion zsh)

# BEGIN SNIPPET: Platform.sh CLI configuration
HOME=${HOME:-'/home/tbuchta'}
export PATH="$HOME/"'.platformsh/bin':"$PATH"
if [ -f "$HOME/"'.platformsh/shell-config.rc' ]; then . "$HOME/"'.platformsh/shell-config.rc'; fi # END SNIPPET

# source ~/.zplug/init.zsh
# zplug "chitoku-k/fzf-zsh-completions"

# heroku autocomplete setup
HEROKU_AC_ZSH_SETUP_PATH=/home/tbuchta/.cache/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;
enable-fzf-tab
