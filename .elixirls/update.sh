#! /bin/sh

elixir_ls_path="$HOME/.elixirls"
cd $elixir_ls_path
curl -fLO https://github.com/elixir-lsp/elixir-ls/releases/latest/download/elixir-ls.zip
unzip elixir-ls.zip
chmod +x $elixir_ls_path/language_server.sh
