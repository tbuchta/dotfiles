require('nvim_comment').setup{
  marker_padding = true,
  comment_empty = true,
  line_mapping = '<Leader>cc',
  operator_mapping = '<Leader>c',
  comment_empty_trim_whitespace = true
}

