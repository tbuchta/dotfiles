local o  = vim.o
local wo = vim.wo
local bo = vim.bo

o.relativenumber = true
o.number = true
o.tabstop = 2
o.smarttab = true
o.shiftwidth = 2
o.expandtab = true

o.autoindent = true
o.copyindent = true

o.splitright = true

o.timeoutlen = 500
o.ttimeoutlen = 0

o.colorcolumn = "100"
o.autoread = true
o.et = true
vim.opt.termguicolors = true

o.updatetime = 300
