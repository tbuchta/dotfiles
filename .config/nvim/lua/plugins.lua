return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'

  use 'terrortylor/nvim-comment'
  use 'tpope/vim-endwise'
  use 'tpope/vim-rhubarb'
  use 'tpope/vim-fugitive'
  use 'tpope/vim-obsession'
  use 'djoshea/vim-autoread'
  use { 'tpope/vim-rails', ft = { 'ruby' } }
  use { 'tpope/vim-bundler', ft = { 'ruby' } }
  use { 'ecomba/vim-ruby-refactoring', ft = { 'ruby' } }
  use { 'vim-ruby/vim-ruby', ft = { 'ruby' } }
  use { 'elixir-lang/vim-elixir', ft = { 'elixir' } }
  use { 'pangloss/vim-javascript', ft = { 'javascript' } }
  use { 'maxmellon/vim-jsx-pretty', ft = { 'javascript' } }
  use { 'junegunn/fzf', run = 'fzf#install()' }
  use 'junegunn/fzf.vim'
  use 'tpope/vim-vinegar'
  use 'christoomey/vim-tmux-navigator'
  use 'xolox/vim-misc'
  use { 'leafgarland/typescript-vim', ft = { 'typescript' } }
  -- Colorschemes
  use { 'tjdevries/colorbuddy.nvim' }
  use { "ellisonleao/gruvbox.nvim" }
  use { 'sainnhe/gruvbox-material' }
  use { 'svrana/neosolarized.nvim' }
  -- use { "catppuccin/nvim", as = "catppuccin" }
  -- End Colorschemes
  use 'kyazdani42/nvim-web-devicons'

  use 'junegunn/vim-easy-align'
  -- Preview marks on pressing `
  use 'Yilin-Yang/vim-markbar'
  use 'windwp/nvim-autopairs'
  use 'windwp/nvim-ts-autotag'
  -- Testing
  use 'janko/vim-test'
  use 'benmills/vimux'
  use { 'spiegela/vimix', ft = { 'elixir' } }
  use { 'pgr0ss/vimux-ruby-test', ft = { 'ruby' } }

  use "b0o/mapx.nvim"

  use 'tpope/vim-repeat'
  use 'tpope/vim-unimpaired'

  use 'sheerun/vim-polyglot'
  use 'easymotion/vim-easymotion'
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use 'tweekmonster/wstrip.vim'

  -- Configs for native neovim lsp
  use "williamboman/mason.nvim" -- Autodownload lsp, dap, linter
  use "williamboman/mason-lspconfig.nvim"
  use 'neovim/nvim-lspconfig'

  use { 'ms-jpq/coq_nvim', branch = 'coq' } -- completion engine
  use { 'ms-jpq/coq.artifacts', branch = 'artifacts' } -- snippets for coq
  -- use { 'ms-jpq/chadtree', branch = 'chad', run = 'python3 -m chadtree deps' }
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use 'nvim-treesitter/nvim-treesitter-context'

  -- Search files/live grep
  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/plenary.nvim'
  use { 'nvim-telescope/telescope.nvim',
    requires = {
      { 'nvim-telescope/telescope-live-grep-raw.nvim' }
    }
  }
  use 'lewis6991/gitsigns.nvim'
  use { 'lukas-reineke/indent-blankline.nvim', branch = 'master' }

  use 'godlygeek/tabular'
  use 'plasticboy/vim-markdown'

  use 'preservim/tagbar'
  use 'folke/zen-mode.nvim'
  use 'editorconfig/editorconfig-vim'
  use 'lukas-reineke/lsp-format.nvim'
  use 'onsails/lspkind.nvim'
  use {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    requires = {
      "nvim-lua/plenary.nvim",
      "kyazdani42/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    }
  }
  use 'jenterkin/vim-autosource'
  use {
    'folke/which-key.nvim',
    config = function()
      require("which-key").setup {}
    end
  }
  use 'folke/trouble.nvim'
  use 'nvim-telescope/telescope-symbols.nvim'
  use({
    "kylechui/nvim-surround",
    config = function()
      require("nvim-surround").setup({
        -- Configuration here, or leave empty to use defaults
      })
    end
  })
  use { 'akinsho/git-conflict.nvim', tag = "*", config = function()
    require('git-conflict').setup()
  end }
  use({
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    config = function()
      require("lsp_lines").setup()
    end,
  })
end)
