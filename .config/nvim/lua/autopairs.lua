require('nvim-autopairs').setup()
require('nvim-ts-autotag').setup { autotag = { enable = true } }
