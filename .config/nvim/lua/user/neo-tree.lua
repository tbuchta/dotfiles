vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])
-- vim.fn.sign_define("DiagnosticSignError", { text = "\uf057 ", texthl = "DiagnosticSignError" })
-- vim.fn.sign_define("DiagnosticSignWarn", { text = "\uf071 ", texthl = "DiagnosticSignWarn" })
-- vim.fn.sign_define("DiagnosticSignInfo", { text = "\uf05a ", texthl = "DiagnosticSignInfo" })
-- vim.fn.sign_define("DiagnosticSignHint", { text = "\uf834", texthl = "DiagnosticSignHint" })

require('neo-tree').setup({
  close_if_last_window = true, -- Close Neo-tree if it is the last window left in the tab
  popup_border_style = "rounded",
  enable_git_status = true,
  enable_diagnostics = false,
  filesystem = {
    hijack_netrw_behavior = "open_current", -- netrw disabled, opening a directory opens within the
    bind_to_cwd = false,
    filtered_items = {
      hide_dotfiles = false,
      hide_gitignored = false,
    }
  },
  window = {
    mappings = {
      ["-"] = "navigate_up",
    }
  }
})
