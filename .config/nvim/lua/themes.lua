local o = vim.o
local g = vim.g

-- latte - light
-- mocha - darkest

-- g.catppuccin_flavour = "mocha" -- latte, frappe, macchiato, mocha


-- require("catppuccin").setup({
--   transparent_backgroud = true,
--   -- term_colors = false,
--   integrations = {
--     treesitter = true,
--     lsp_trouble = true,
--     gitsigns = true,
--     telescope = true,
--     which_key = true,
--     indent_blankline = {
--       enabled = true,
--       colored_indent_levels = false,
--     },
--     vim_sneak = true,
--   },
-- })

-- o.background = "light" -- or "light" for light mode
o.background = "dark" -- or "light" for light mode

g['gruvbox_contrast_light'] = 'soft'
g['gruvbox_contrast_dark'] = 'medium'
g['gruvbox_material_background'] = 'medium'
g['gruvbox_contrast_dark'] = 'medium'

-- require('neosolarized').setup({
--   comment_italics = true,
-- })


vim.cmd([[colorscheme gruvbox]])

-- vim.cmd [[colorscheme catppuccin]]
