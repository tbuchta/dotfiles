local map = vim.api.nvim_set_keymap
local coq = require "coq"

require("zen-mode").setup {}
require("trouble").setup {}
require("telescope").load_extension("live_grep_args")

vim.g['wstrip_auto'] = 1

vim.g['NERDSpaceDelims'] = 1
vim.g['NERDCompactSexyComs'] = 1
vim.g['NERDDefaultAlign'] = 'left'
vim.g['NERDCommentEmptyLines'] = 1
vim.g['NERDTrimTrailingWhitespace'] = 1
vim.g['NERDCreateDefaultMappings'] = 0

local m = require('mapx').setup { global = "force" }
-- keymaps
m.nnoremap('<Leader>g', '<cmd>Git<CR>')
m.nnoremap('<leader>cc', ':CommentToggle<CR>')
-- -- Find files using Telescope command-line sugar.
m.nnoremap('<C-p>', '<cmd>Telescope find_files<cr>')
-- m.nnoremap('<leader>rr', '<cmd>lua require("telescope").extensions.live_grep_args.live_grep_raw()<CR>')
m.nnoremap('<leader>fg', '<cmd>lua require("telescope").extensions.live_grep_args.live_grep_args()<CR>')
-- m.nnoremap('<leader>fb', '<cmd>Telescope buffers<CR>')
m.nnoremap('<leader>fb', '<cmd>lua require("telescope.builtin").buffers()<CR>')
m.nnoremap('<leader>rr', '<cmd>Telescope resume<cr>')
m.nnoremap('<leader>fh', '<cmd>Telescope help_tags<cr>')
m.nnoremap('<leader>ca', '<cmd>lua require("telescope.builtin").lsp_code_actions()<CR>')
m.nnoremap('<silent>g?', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
m.map('<leader>rf', ':TestNearest<ENTER>')
m.map('<leader>rl', ':TestLast<ENTER>')
m.map('<leader>rb', ':TestFile<ENTER>')
m.map('<leader>f', ':TestSuite<ENTER>')

-- end keymaps

vim.api.nvim_command('COQnow')

-- :let test#strategy = "vimux"

vim.g['test#strategy'] = "vimux"
vim.g['vimux_ruby_cmd_unit_test'] = "RAILS_ENV=test bundle exec bin/rspec"
vim.g['vimux_ruby_file_relative_paths'] = 1
vim.g['VimuxRunnerType'] = "pane"
vim.g['vroom_command_prefix'] = "dc exec web RAILS_ENV=test bundle exec"
vim.g['vroom_use_vimux'] = 1

vim.g['gruvbox_contrast_light'] = 'soft'
vim.g['gruvbox_contrast_dark'] = 'medium'
vim.g['gruvbox_material_background'] = 'medium'
vim.g['gruvbox_contrast_dark'] = 'medium'

require('neosolarized').setup({
  comment_italics = true,
})

-- vim.api.nvim_create_autocmd("*",{command = "<cmd>Obsession<CR>"})
