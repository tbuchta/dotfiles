#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
#polybar base &

sleep .5

current_display_setup=$(autorandr --current)
if [[ "$current_display_setup" == *"home" ]]; then
  echo "Detected special display setup home"
  # Custom setup for home because the right monitor is vertical
  MONITOR="DP-1.2" polybar --reload base & # main 34 inch
  MONITOR="DP-1.3" polybar --reload vertical & # right screen
  MONITOR="DP-2"   polybar --reload laptop & # laptop screen
elif [[ "$current_display_setup" == *"home2" ]]; then
  echo "Detected special display setup home"
  # Custom setup for home because the right monitor is vertical
  MONITOR="DP-0.2" polybar --reload base & # main 34 inch
  MONITOR="DP-0.3" polybar --reload vertical & # right screen
  MONITOR="DP-2"   polybar --reload laptop & # laptop screen
else
  echo "Else"
  # Same bar on each screen
  for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload laptop &
  done
fi
